//
//  TrafficLight.swift
//  Traffic Lights
//
//  Created by Amir Kamali on 4/14/17.
//  Copyright © 2017 Amir Kamali. All rights reserved.
//

import Foundation
import RxSwift
import SceneKit
class StandardTrafficLight:TrafficLightProtocol
{
    
    var stateDelayTimes: [Int]  = [10,3,2]//Delay after switching to each ligh (with order: Green,Yellow,Red)
    lazy var observableObject: Observable<LightStatus>? = nil
    var Name: String
    var state : LightStatus
    var sceneNodes:[SCNNode]?
    private var subscription:Disposable?
    
    required init( lightName:String) {
        self.Name = lightName
        self.state = .Red
        
    }
    convenience required init(lightName:String,suggestedStateTimes:[Int]) {
        self.init(lightName: lightName)
        self.stateDelayTimes = suggestedStateTimes
    }
    
    func addSceneNode(nodes:[SCNNode]?)
    {
        if (sceneNodes == nil)
        {
            sceneNodes = [SCNNode]()
        }
        for i in nodes!
        {
            
            sceneNodes?.append(i)
            setNodeLight(node: i, newStatus: .Red)
        }
        
    }
    func SignalStart() {
        setLight(newStatus: .Red)
    }


    func SignalGreen() -> Int {
        
        if (observableObject == nil)
        {
            
            let g1 = Observable<LightStatus>.just(.Green)
            let d1 = Observable<LightStatus>.empty().delay(RxTimeInterval(stateDelayTimes[0]), scheduler: MainScheduler.instance)
            
            let y1 = Observable<LightStatus>.just(.Yellow)
            
            let d2 = Observable<LightStatus>.empty().delay(RxTimeInterval(stateDelayTimes[1]), scheduler: MainScheduler.instance)
            let r1 = Observable<LightStatus>.just(.Red)
            let d3 = Observable<LightStatus>.empty().delay(RxTimeInterval(stateDelayTimes[2]), scheduler: MainScheduler.instance)
            
            observableObject = g1.concat(d1).concat(y1).concat(d2).concat(r1).concat(d3)
        }
        subscription =   observableObject!.subscribe { event in
            if (!event.isCompleted)
            {
                self.state = event.element!
                self.setLight(newStatus: self.state)
                print("light \(self.Name) turning \(self.state)")
            }
            
            
        }
        
        return stateDelayTimes.reduce(0, +)
    }
    
    //Sets the light to initialized mode
    func SignalStop()
    {
        self.setLight(newStatus: .Red)
        subscription?.dispose()
    }
    func setLight(newStatus:LightStatus)
    {
        if (sceneNodes == nil)
        {
            return
        }
        for i in sceneNodes!
        {
            setNodeLight(node: i, newStatus: newStatus)
        }
    }
    func setNodeLight(node:SCNNode, newStatus:LightStatus)
    {
        if let light = node.light
        {
            DispatchQueue.main.async {
                light.color = LightColors.fromState(state: newStatus)
            }
            
        }
    }
    
    
    
}
