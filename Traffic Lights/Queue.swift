//
//  Queue.swift
//  Traffic Lights
//
//  Created by Amir Kamali on 4/15/17.
//  Copyright © 2017 Amir Kamali. All rights reserved
//
// First-in first-out queue (FIFO)
// New elements are added to the end of the queue. Dequeuing pulls elements from
// the front of the queue.
// Enqueuing and dequeuing are O(1) operations.
//

import Foundation
import UIKit

public struct Queue<T> {
    var allItems = [T?]()
    fileprivate var head = 0
    
    public var isEmpty: Bool {
        
        return count == 0
        
    }
    
    public var count: Int {
        return allItems.count - head
    }
    
    public mutating func enqueue(_ element: T) {
        allItems.append(element)
    }
    
    public mutating func dequeue() -> T? {
        guard head < allItems.count, let element = allItems[head] else { return nil }
        
        allItems[head] = nil
        head += 1
        
        let percentage = Double(head)/Double(allItems.count)
        if allItems.count > 50 && percentage > 0.25 {
            allItems.removeFirst(head)
            head = 0
        }
        
        return element
    }
    
    public var front: T? {
        if isEmpty {
            return nil
        } else {
            return allItems[head]
        }
    }
}
