//
//  TrafficLightColors.swift
//  Traffic Lights
//
//  Created by Amir Kamali on 4/17/17.
//
//

import Foundation
import UIKit

struct LightColors {
    static let COLOR_Red_Light = UIColor.red
    static let COLOR_Green_Light = UIColor.green
    static let COLOR_Yellow_Light = UIColor.orange
    static let COLOR_Unknown_Light = UIColor.lightGray
    static func fromState(state:LightStatus) ->UIColor
    {
        switch state
        {
        case .Red:
            return COLOR_Red_Light
        case .Green:
            return COLOR_Green_Light
        case .Yellow:
            return COLOR_Yellow_Light
        default:
            return COLOR_Unknown_Light
        }
    }
}
