//
//  lightStatus.swift
//  Traffic Lights
//
//  Created by Amir Kamali on 4/14/17.
//  Copyright © 2017 Amir Kamali. All rights reserved.
//

import Foundation

enum LightStatus
{
    case Red
    case Yellow
    case Green
    case Unknown
}
