//
//  TrafficController.swift
//  Traffic Lights
//
//  Created by Amir Kamali on 4/14/17.
//  Copyright © 2017 Amir Kamali. All rights reserved.
//

import Foundation
import RxSwift
class TrafficControllerEngine:TrafficLightEngineProtocol
{
    var engineStarted:Bool
    var LightsQueue:Queue<TrafficLightProtocol>
    var scheduler : SerialDispatchQueueScheduler?
    var engine_observable : Observable<Int>?
    private var engine_subscription : Disposable?
    let GreenLight_Duration = 25
    let Light_States = [25,5]
    var selectAndSignalDelegate:(()->Int)?
    
    init()
    {
        LightsQueue = Queue<TrafficLightProtocol>()
        engineStarted = false
        selectAndSignalDelegate = defaultSelectAndSignalDelegate
        print("Engine initialized")
    }
    
    func start()
    {
        scheduler =  SerialDispatchQueueScheduler(qos: .default)
        print("Engine started")
        for i in LightsQueue.allItems
        {
            i?.SignalStart()
        }
        
        
        //Generate clock for the app
        engine_observable =  Observable<Int>.repeatElement(0, scheduler: scheduler!)
        engine_subscription =  engine_observable!.subscribe { event in
            if (self.LightsQueue.count == 0)
            {
                return
            }
            
            let delay = self.selectAndSignalDelegate?() ?? 1
            
            
            Thread.sleep(forTimeInterval: TimeInterval(delay))
        }
        engineStarted = true
    }
    //Method that selects the next light and signals to become green
    func defaultSelectAndSignalDelegate()->Int
    {
        let currentLight = LightsQueue.dequeue()!
        
        let delay = currentLight.SignalGreen()
        
        LightsQueue.enqueue(currentLight)
        
        return delay
        
    }
    //Method to stop the entire scene
    func stop()
    {
        
        for i in LightsQueue.allItems
        {
            i?.SignalStop()
        }
        
        engine_subscription?.dispose()
        engineStarted = false
        print("Engine stopped")
    }
    
}
