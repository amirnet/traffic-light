//
//  Constants.swift
//  Traffic Lights
//
//  Created by Amir Kamali on 4/17/17.
//  Copyright © 2017 Amir Kamali. All rights reserved.
//

import Foundation
import UIKit

let APP_GREEN_LIGHT_DURATION = 25
let APP_YELLOW_LIGHT_DURATION = 5

// 2 seconds delay after current light becomes red 
//and before next light becomes green,  
//(Change to zero for 30 sec green /30 sec light time
let APP_DELAY_BEFORE_SWITCH_NEXT_LIGHT = 2

