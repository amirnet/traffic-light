//
//  Providers.swift
//  Traffic Lights
//
//  Created by Amir Kamali on 4/17/17.
//
//

import Foundation

struct AppProviders
{
    static var TrafficEngineProvider:TrafficLightEngineProtocol
    {
        get{ return TrafficControllerEngine()}
    }
    static func LightProvider(lightName:String,suggestedStateTimes:[Int])->TrafficLightProtocol
    {
        return StandardTrafficLight(lightName: lightName,suggestedStateTimes:suggestedStateTimes)
    }
 
}
