//
//  ViewController.swift
//  Traffic Lights
//
//  Created by Amir Kamali on 4/13/17.
//  Copyright © 2017 Amir Kamali. All rights reserved.
//

import UIKit
import RxSwift
import SceneKit
import RxCocoa
class ViewController: UIViewController {
    
    @IBOutlet  var sceneView: SCNView!
    @IBOutlet weak var btn_switchStartStop: UIButton!
    
    var engine:TrafficLightEngineProtocol?
    var cameraOrbit:SCNNode?
    var lastWidthRatio: Float = 0
    var lastHeightRatio: Float = 0
    var zoomLimits_min:Float = 12;
    var zoomLimits_max:Float = 85;
    var startScale:CGFloat = 1
    var lastScale:CGFloat = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Initialize Scene
        sceneView.showsStatistics = true
        AttachGestures()
        cameraOrbit = sceneView.scene?.rootNode.childNode(withName: "camera", recursively: true)
        _ = btn_switchStartStop.rx.tap.map {event ->  Bool in (self.engine?.engineStarted)!}
            .subscribe{event in self.SetEngineStartStop(state: event.element)
        }
        
        //Initialize Engine
        initEngine()
        
    }
    
    func initEngine()
    {
        engine = AppProviders.TrafficEngineProvider
        
        let t1 = AppProviders.LightProvider( lightName: "North-South",suggestedStateTimes:[APP_GREEN_LIGHT_DURATION,APP_YELLOW_LIGHT_DURATION,APP_DELAY_BEFORE_SWITCH_NEXT_LIGHT])
        let nslight = sceneView.scene?.rootNode.childNode(withName: "light_north_south", recursively: true)
        let nslight2 = sceneView.scene?.rootNode.childNode(withName: "light_north_south2", recursively: true)
        let nslight3 = sceneView.scene?.rootNode.childNode(withName: "light_north_south3", recursively: true)
        t1.addSceneNode(nodes: [nslight!,nslight2!,nslight3!])
        
        
        let t2 = AppProviders.LightProvider(lightName: "East-West",suggestedStateTimes:[APP_GREEN_LIGHT_DURATION,APP_YELLOW_LIGHT_DURATION,APP_DELAY_BEFORE_SWITCH_NEXT_LIGHT])
        let ewlight = sceneView.scene?.rootNode.childNode(withName: "light_east_west", recursively: true)
        let ewlight2 = sceneView.scene?.rootNode.childNode(withName: "light_east_west2", recursively: true)
        let ewlight3 = sceneView.scene?.rootNode.childNode(withName: "light_east_west3", recursively: true)
        t2.addSceneNode(nodes: [ewlight!,ewlight2!,ewlight3!])
        
        
        engine!.LightsQueue.enqueue(t1)
        engine!.LightsQueue.enqueue(t2)
    }
    func AttachGestures()
    {
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(handleTap))
        panGesture.cancelsTouchesInView = false;
        sceneView!.addGestureRecognizer(panGesture);
        
        let ZoomGesture = UIPinchGestureRecognizer(target: self, action: #selector(pinchGesture))
        ZoomGesture.cancelsTouchesInView = false;
        sceneView!.addGestureRecognizer(ZoomGesture);
        
    }
    func handleTap(sender: UIPanGestureRecognizer) {
        
        let translation = sender.translation(in: sender.view!)
        let widthRatio = Float(translation.x) / Float(sender.view!.frame.size.width) + lastWidthRatio
        let heightRatio = Float(translation.y) / Float(sender.view!.frame.size.height) + lastHeightRatio
        self.cameraOrbit?.eulerAngles.z = Float(-2 * Double.pi) * widthRatio
        if (sender.state == .ended) {
            lastWidthRatio = widthRatio.truncatingRemainder(dividingBy:  1)
            lastHeightRatio = heightRatio.truncatingRemainder(dividingBy: ( 1))
        }
    }
    
    func pinchGesture(sender: UIPinchGestureRecognizer)
    {
        if sender.numberOfTouches == 2 {
            
            // print("X1")
            let zoom = sender.scale
            if (sender.state == .began){
                startScale = lastScale
                
            }
            else if (sender.state == .changed){
                let zoom = sender.scale
                
                var z = Float(50/(startScale * zoom))
                
                if (z > zoomLimits_max)
                {
                    
                    z = zoomLimits_max
                }
                else if (z < zoomLimits_min)
                {
                    
                    z = zoomLimits_min
                }
                
                cameraOrbit?.camera!.yFov = Double(z)
            }
            else {
                
                lastScale = startScale * zoom
            }
            if (startScale < 1)
            {
                startScale = 1
            }
            if (lastScale < 1)
            {
                lastScale = 1
            }
        }
        
        
    }
    func SetEngineStartStop(state:Bool?)
    {
        if (state == nil)
        {
            return
        }
        else  if (state)!
        {
            engine!.stop()
            btn_switchStartStop.setTitle("Start", for: .normal)
        }
        else if !(state!)
        {
            engine!.start()
            btn_switchStartStop.setTitle("Stop", for: .normal)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

