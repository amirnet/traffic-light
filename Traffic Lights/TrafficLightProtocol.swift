//
//  TrafficLightProtocol.swift
//  Traffic Lights
//
//  Created by Amir Kamali on 4/15/17.
//  Copyright © 2017 Amir Kamali. All rights reserved.
//

import Foundation
import SceneKit
protocol TrafficLightProtocol
{
    var Name:String {get set}
    var stateDelayTimes:[Int] {get set}
    var state : LightStatus {get set}
    var sceneNodes:[SCNNode]? {get set}
    func addSceneNode(nodes:[SCNNode]?);
    
    init(lightName:String,suggestedStateTimes:[Int])
    init(lightName:String)
    
    /*
     Tells the light to become green
    Depending on the type of light,
    Light will return duration till going back to red again.
    Usually (Green duration + yellow duration)
     */
    func SignalGreen() -> Int
  
    //Initialize / Deinitialize light
    func SignalStop()
    func SignalStart()
}
