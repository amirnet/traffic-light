//
//  TrafficLightEngineProtocol.swift
//  Traffic Lights
//
//  Created by Amir Kamali on 4/17/17.
//  Copyright © 2017 Amir Kamali. All rights reserved.
//

import Foundation
protocol TrafficLightEngineProtocol {
    var LightsQueue:Queue<TrafficLightProtocol> {get set}
    var engineStarted:Bool {get set}
    func stop()
    func start()
}
