Traffic Light Simulator 

Implementation highlights:

RxSwift

SceneKit

The app simulates traffic lights as requested in requirement document

Brief notes:

The app idea sounds very simple. However I found there are many possibile 
strategies to implement it. In my implementation, there are two major components
TrafficControllerEngine and TrafficLight which are implementation of their protocol.
I also wanted to have some fun with my coding so I decided to implement this app
in 3D. 

The idea behind the implmentation is simple, two lights are initialized and enqueued in
a list; Lights have a default state of Red. Engine will start by dequeuing and 
notifying a light to switch state to become Green,Yellow and goes to Red again. Engine will
enqueue the light for the next round. This process will continue in a loop process
unless it is interrupted by start/stop button. 

Each light is initialized by their name and a suggested states duration that represent
each state's duration (Green, Yellow, Red). After recieving signal, TrafficLight uses
time parameters to determine and return the total duration of each state changes until
going back to default state. Engine uses the returned value to determine amount of time
needed to wait before dequeue next light and issuing the next signal. In my implementation
default durations are defined in Constant.swift. 

TrafficLight class contains a property "sceneNodes" that holds the reference to the actual
nodes in 3D scene. This allows the light to have a same light in different part of the scene.


The reason I chose this implementation is because it was easy and allows the software
to be extended easily in future by enqueueing more lights. The app will also prints the
lights internal states in the console in case it helps to understand app's lifecycle better. 


Unit Tests:

I have implemented several unit tests to show testing of Engine and lights components. 
I tried to demonstrate XCTest, asynchronous task tests, etc. More tests could be done
if I had more time over this project.


In this project I tried to comment my codes as much as possible. In case there is any 
problem or question, feel free to contact me Amir.n3t@gmail.com

All the best

Amir Kamali

