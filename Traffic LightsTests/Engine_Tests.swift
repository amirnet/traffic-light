//
//  Engine_Test.swift
//  Traffic Lights
//
//  Created by Amir Kamali on 4/17/17.
//  Copyright © 2017 Amir Kamali. All rights reserved.
//

import XCTest
@testable import Traffic_Lights

class Engine_Tests: XCTestCase {
    
    
    override func setUp() {
        super.setUp()
        
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testEngineStartStopStates() {
        
        var engine:TrafficLightEngineProtocol = TrafficControllerEngine()
        let preState = engine.engineStarted
        XCTAssertEqual(preState, false)
        engine.start()
        let startState = engine.engineStarted
        XCTAssertEqual(startState, true)
        
        engine.stop()
        let finishState = engine.engineStarted
        XCTAssertEqual(finishState, false)
        
    }
    func testEngineLightSignals()
    {
        var expectation_callcount = expectation(description: "call count is equal to 5")
        var callCount = 0
        func fakeSelectAndSignal() -> Int
        {
            callCount = callCount + 1
            if (callCount == 5)
            {
                expectation_callcount.fulfill()
            }
            return 1
        }
        
        let engine:TrafficControllerEngine = TrafficControllerEngine()
        
        engine.LightsQueue.enqueue(StandardTrafficLight_Fake(lightName: "FK1", suggestedStateTimes: [1,1,0]))
        engine.LightsQueue.enqueue(StandardTrafficLight_Fake(lightName: "FK2", suggestedStateTimes: [1,1,0]))
        engine.selectAndSignalDelegate = fakeSelectAndSignal
        engine.start()
        
        wait(for: [expectation_callcount], timeout: 5)
    }
    
    func testDefaultSelectAndSignal()
    {
        let engine:TrafficControllerEngine = TrafficControllerEngine()
        
        engine.LightsQueue.enqueue(StandardTrafficLight_Fake(lightName: "FK1", suggestedStateTimes: [2,3,0]))
        engine.LightsQueue.enqueue(StandardTrafficLight_Fake(lightName: "FK2", suggestedStateTimes: [4,5,0]))

        let value1 = engine.defaultSelectAndSignalDelegate()
        XCTAssertEqual(value1, 5)
        XCTAssertEqual(engine.LightsQueue.front?.Name, "FK2")
        
        //Check if rotation is working
        let value2 = engine.defaultSelectAndSignalDelegate()
        XCTAssertEqual(value2, 9)
        XCTAssertEqual(engine.LightsQueue.front?.Name, "FK1")
        
    }

}
