//
//  FakeLight.swift
//  Traffic Lights
//
//  Created by Amir Kamali on 4/17/17.
//  Copyright © 2017 Amir Kamali. All rights reserved.
//

import Foundation
import SceneKit
@testable import Traffic_Lights

class StandardTrafficLight_Fake:TrafficLightProtocol
{
    var stateDelayTimes: [Int]  = [10,3,0]//Delay after switching to each ligh (with order: Green,Yellow,Red)
    var Name: String
    var state : LightStatus
    var sceneNodes:[SCNNode]?
    
    required init( lightName:String) {
        self.Name = lightName
        self.state = .Red
        
    }
    convenience required init(lightName:String,suggestedStateTimes:[Int]) {
        self.init(lightName: lightName)
        self.stateDelayTimes = suggestedStateTimes
    }
    
    
    func addSceneNode(nodes:[SCNNode]?)
    {
        if (sceneNodes == nil)
        {
            sceneNodes = [SCNNode]()
        }
        for i in nodes!
        {
            
            sceneNodes?.append(i)
            setNodeLight(node: i, newStatus: .Unknown)
        }
        
    }
    func SignalStart() {
        setLight(newStatus: .Red)
    }
   

    func SignalGreen() -> Int {
        return stateDelayTimes.reduce(0, +)
    }
    //Sets the light to initialized mode
    func SignalStop()
    {
        self.setLight(newStatus: .Unknown)
       
    }
    func setLight(newStatus:LightStatus)
    {
        if (sceneNodes == nil)
        {
            return
        }
        for i in sceneNodes!
        {
            setNodeLight(node: i, newStatus: newStatus)
        }
    }
    func setNodeLight(node:SCNNode, newStatus:LightStatus)
    {
        print("Light nodes for TrafficLight \(Name) updated to\(newStatus)")
    }
}
