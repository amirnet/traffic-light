//
//  Light_Tests.swift
//  Traffic Lights
//
//  Created by Amir Kamali on 4/17/17.
//  Copyright © 2017 Amir Kamali. All rights reserved.


import XCTest
@testable import Traffic_Lights

class Light_Test: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_verify_Name() {
        
        let testLight = StandardTrafficLight(lightName:"test")
        XCTAssertEqual("test", testLight.Name)
      
    }
    func test_verify_StateTimes()
    {
        let testLight2 = StandardTrafficLight(lightName:"test",suggestedStateTimes:[1,2,0])
       
        XCTAssertEqual(testLight2.stateDelayTimes, [1,2,0])
    }
    func test_verify_InitialState()
    {
        let testLight = StandardTrafficLight(lightName:"test",suggestedStateTimes:[1,2,0])
        //Make sure light starts with red
        XCTAssertEqual(LightStatus.Red, testLight.state)
    }
   
    func test_SignalGreen_ReturnedTime() {

        let testLight = StandardTrafficLight(lightName:"test",suggestedStateTimes:[1,2,0])
        let availableTime = testLight.SignalGreen()
        XCTAssertEqual(3, availableTime)
    }
    func test_SignalGreen_LightSwitchTest() {
        
        let testLight = StandardTrafficLight(lightName:"test1",suggestedStateTimes:[1,2,0])
        //Initial states
        var turnedYellow  = false
        var turnedGreen = false
        var turnedBackRed = false
        
        let expect = expectation(description: "light switch in order green-yellow-red")
        _ = testLight.SignalGreen()
        _ = testLight.observableObject?.subscribe
            {  event in
                
                //Make sure lights turn green-yellow-red in order
                if (!event.isCompleted)
                {
                    if (!turnedGreen && !turnedYellow && !turnedBackRed && event.element! == .Green)
                    {
                        turnedGreen = true
                    }
                    else if (turnedGreen && !turnedYellow && !turnedBackRed && event.element! == .Yellow)
                    {
                        turnedYellow = true
                    }
                    else if (turnedGreen && turnedYellow && !turnedBackRed && event.element! == .Red)
                    {
                        turnedBackRed = true
                    }
                    else
                    {
                        turnedBackRed = false
                        turnedYellow = false
                        turnedGreen = false
                        XCTAssert(false, "Invalid light state")
                    }
                }
                else
                {
                    if (testLight.state == .Red && turnedGreen && turnedYellow && turnedBackRed)
                    {
                        expect.fulfill()
                    }
                }
        }
        let totalTime = Double( testLight.stateDelayTimes.reduce(0, + )) + 0.5
        wait(for: [expect], timeout: TimeInterval(totalTime))
        
    }
    func test_SignalStart()
    {
        let testLight = StandardTrafficLight(lightName:"test",suggestedStateTimes:[1,2,0])
        testLight.setLight(newStatus: .Yellow)
        testLight.SignalStart()
        //Make sure light starts with red
        XCTAssertEqual(LightStatus.Red, testLight.state)
    }
    func test_SignalStop()
    {
        let testLight = StandardTrafficLight(lightName:"test",suggestedStateTimes:[1,2,0])
        testLight.setLight(newStatus: .Yellow)
        testLight.SignalStop()
        //Make sure light starts with red
        XCTAssertEqual(LightStatus.Red, testLight.state)
    }
    
}
